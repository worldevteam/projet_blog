<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/



Route::name('blogs_path')->get('/', 'BlogsController@index');
Route::name('create_path')->get('/blog/create', 'BlogsController@create');
Route::name('store_blog_path')->post('/blog', 'BlogsController@store');
Route::name('blogs_path_show')->get('/blog/{id}', 'BlogsController@show');
Route::name('blogs_path_edit')->get('/blog/{id}/edit', 'BlogsController@edit');
Route::name('blog_path_update')->put('/blog/{id}/', 'BlogsController@update');
Route::name('blog_path_delete')->delete('/blog/{id}/', 'BlogsController@destroy');
Route::name('category_path')->get('/category', 'CategoriesController@index');
Route::name('store_category_path')->post('/category', 'CategoriesController@store');

Route::name('create_path')->get('/blog/create', 'BlogsController@cate');



Auth::routes();

Route::get('/home', 'HomeController@index')->name('home');
